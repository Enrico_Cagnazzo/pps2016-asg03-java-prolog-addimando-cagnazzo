package u07asg;

import java.util.List;


public interface Sudoku {

    void createTable();

    void setNumber(int i, int j, int number);

    boolean canPut(int i, int j, int number);

    boolean checkCompleted();

    List getBoard();

    void deleteNumber(int i, int j);
}
