package u07asg;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Timer;
import java.util.ArrayList;
import java.util.TimerTask;

public class SudokuApp {

    private final Sudoku sudoku;
    private final JTextField[][] boardCells = new JTextField[9][9];
    private final JButton exit = new JButton("Exit");
    private final JFrame frame = new JFrame("SUDOKU - Assignment03");

    public SudokuApp(Sudoku sudoku) throws Exception {
        this.sudoku=sudoku;
        initPane();
    }

    private void initPane() {
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        JPanel b=new JPanel(new GridLayout(9,9));
        ArrayList<Integer> board = new ArrayList(sudoku.getBoard());
        for (int i=0;i<9;i++){
            for (int j=0;j<9;j++) {
                final int i2 = i;
                final int j2 = j;
                int value = board.get(i + j * 9);
                Font font = new Font("SansSerif",Font.PLAIN, 20);
                boardCells[i][j] = new JTextField(value == 0 ? "" : Integer.toString(value));
                b.add(boardCells[i][j]);
                boardCells[i][j].setEnabled(value == 0);
                boardCells[i][j].setFont(font);
                boardCells[i][j].setDisabledTextColor(Color.BLACK);
                boardCells[i][j].setHorizontalAlignment(JTextField.CENTER);
                int block = i2/3 + j2/3 *3;
                boardCells[i][j].setBackground(block%2==0?Color.cyan:Color.white);

                boardCells[i][j].addActionListener((ActionEvent e) -> {
                    if (!boardCells[i2][j2].getText().equals("")) {
                        int number;
                        try {
                            do {
                                number = Integer.parseInt(boardCells[i2][j2].getText());
                                if(number <=0 || number > 9){
                                    boardCells[i2][j2].setText("");
                                }
                            } while (!(number > 0 && number <= 9));

                        putNumber(i2, j2, number);
                        }catch (NumberFormatException e1){
                            JOptionPane.showMessageDialog(null,"Insert only numbers between 1-9!","Warning",JOptionPane.WARNING_MESSAGE);
                            boardCells[i2][j2].setText("");
                        }
                    } else {
                        deleteNumber(i2, j2);
                    }
                });
            }
        }
        JPanel s=new JPanel(new FlowLayout());
        s.add(exit);
        exit.addActionListener(e -> System.exit(0));
        frame.add(BorderLayout.CENTER,b);
        frame.add(BorderLayout.SOUTH,s);
        frame.setSize(500,580);
        frame.setVisible(true);
    }

    private void putNumber(int i, int j, int number) {
        if (sudoku.canPut(i,j,number)){
            boardCells[i][j].setText(Integer.toString(number));
            sudoku.setNumber(i,j,number);
        }else {
            boardCells[i][j].setText("");
            boardCells[i][j].setBackground(Color.red);
            Timer timer = new Timer();
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    int block = i/3 + j/3 *3;
                    boardCells[i][j].setBackground(block%2==0?Color.cyan:Color.white);
                }
            };
            timer.schedule(task,500);
        }
        if (sudoku.checkCompleted()){
            exit.setText("Completed!");
        }
    }

    private void deleteNumber(int i, int j) {
        sudoku.deleteNumber(i,j);
        boardCells[i][j].setText("");
    }

    public static void main(String[] args){
        try {
            new SudokuApp(new SudokuImpl("src/u07asg/sudoku.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
            e.printStackTrace();
        }
    }
}
