package u07asg


import alice.tuprolog.{Struct, Theory}
import u07asg.Scala2P._
import java.io.FileInputStream
import java.util

import collection.JavaConverters._
import scala.collection.mutable


class SudokuImpl (fileName: String) extends Sudoku{
    private val engine = mkPrologEngine(new Theory(new FileInputStream(fileName)))
    createTable()

    override def checkCompleted(): Boolean = {
        val goal = "board(B),completed(B)"
        solveWithSuccess(engine, goal)
    }

    override def createTable(): Unit ={
        val goal = "retractall(board(_)),create_board(B),assert(board(B))"
        solveWithSuccess(engine,goal)
        fillTable()
    }

    private def fillTable():Unit = {
        setNumber(0,2,8)
        setNumber(0,5,4)
        setNumber(0,6,3)
        setNumber(0,7,6)
        setNumber(1,1,2)
        setNumber(1,4,5)
        setNumber(1,5,9)
        setNumber(1,7,1)
        setNumber(2,0,4)
        setNumber(2,2,3)
        setNumber(2,8,2)
        setNumber(3,1,6)
        setNumber(3,3,2)
        setNumber(3,4,9)
        setNumber(3,6,4)
        setNumber(4,0,5)
        setNumber(4,1,8)
        setNumber(4,2,2)
        setNumber(4,3,4)
        setNumber(5,4,6)
        setNumber(5,6,2)
        setNumber(5,7,5)
        setNumber(6,2,1)
        setNumber(6,4,7)
        setNumber(6,8,3)
        setNumber(7,1,3)
        setNumber(7,3,1)
        setNumber(7,5,8)
        setNumber(8,0,2)
        setNumber(8,6,1)
        setNumber(8,7,4)
        setNumber(8,8,8)
    }


    override def getBoard: util.List[Int] = {
        val term = solveOneAndGetTerm(engine, "board(B)", "B").asInstanceOf[Struct]
        val iterator = term.listIterator()
        iterator.asScala.toList.map(_.toString).map(Integer.parseInt).to[mutable.Buffer].asJava
    }

    override def setNumber(i: Int, j: Int, number: Int): Unit = {
        val goal = s"retract(board(BI)),setNumber(BO,BI,${i+j*9},$number),asserta(board(BO))"
        solveWithSuccess(engine,goal)
    }



    override def canPut(i: Int, j: Int, number:Int): Boolean = {
        isEmpty(i+j*9) && checkRow(i,number) && checkColumn(j,number) && checkBlock((i/3)+(j/3)*3,number)
    }

    def isEmpty(i: Int):Boolean={
        val goal = s"board(B),isEmpty(B,$i)"
        solveWithSuccess(engine,goal)
    }

    def checkRow(i: Int, number: Int): Boolean = {
        val goal = s"board(B),checkRow(B,$i,$number)"
        solveWithSuccess(engine,goal)
    }

    def checkColumn(j: Int, number: Int): Boolean = {
        val goal = s"board(B),checkColumn(B,${j*9},$number)"
        solveWithSuccess(engine,goal)
    }

    def checkBlock(i: Int, number: Int): Boolean = {
        val goal = s"board(B),checkBlock(B,$i,$number)"
        solveWithSuccess(engine,goal)
    }

    override def deleteNumber(i: Int, j: Int): Unit = {
        val goal = s"retract(board(BI)),deleteNumber(BO,BI,${i+j*9}),asserta(board(BO))"
        solveWithSuccess(engine,goal)
    }
}
