% create_board(-Board): creates an initially empty board
create_board(B):- retractall(board(_)), create_list(81,0,B), assert(board(B)).
create_list(0,_,[]) :- !.
create_list(N,X,[X|T]) :- N2 is N-1, create_list(N2,X,T).


% completed(+Board): check if the board is completed
completed([]).
completed([H|T]):- H =\= 0, completed(T).

% isEmpty(+Board,+Pos): check if the cell in position pos is empty
isEmpty([0|_],0) :- !.
isEmpty([_|T],N) :- N2 is N-1, isEmpty(T,N2).

%setNumber(-Board,+OldBoard,+Pos,+Number): update the board with new value inserted
setNumber([X|T],[_|T],0,X).
setNumber([H|R],[H|T],I,X):- I1 is I-1, setNumber(R,T,I1,X).

%checkColumn(+Board,+Column,+Number): check the columns in order to find replicates
checkColumn(_,-9,_) :- !.
checkColumn([_|T],I,N) :- I2 is I-1,I>0,!, checkColumn(T,I2,N).
checkColumn([H|T],I,N) :- I2 is I-1, H =\= N,checkColumn(T,I2,N).

%checkRow(+Board,+Row,+Number): check the rows in order to find replicates
checkRow([],_,_).
checkRow([H|T],0,N) :- !, H =\= N,checkRow(T,8,N).
checkRow([_|T],I,N):- I2 is I-1, checkRow(T,I2,N).

%checkBlock(+Board,+Block,+Number): check if there is the number in the block
checkBlock(B,BL,N):- I is (BL/3)*27+(BL mod 3)*3 ,checkBlock(B,I,3,N).
checkBlock(_,_,0,_):- !.
checkBlock(B,-3,R,N):- !, R2 is R-1, checkBlock(B,6,R2,N).
checkBlock([H|T],I,R,N):- I>0, !, I2 is I-1, checkBlock(T,I2,R,N).
checkBlock([H|T],I,R,N):- H=\=N, I2 is I-1, checkBlock(T,I2,R,N).

%deleteNumber(-Board,+OldBoard,+Pos): remove the element in position Pos
deleteNumber([0|T],[_|T],0).
deleteNumber([H|R],[H|T],I):- I1 is I-1, deleteNumber(R,T,I1).